var searchData=
[
  ['reactants',['reactants',['../structderivedtype_1_1reaction.html#a89b29ab6586c10cb3dbb9dad938b2d7c',1,'derivedtype::reaction::reactants()'],['../structderivedtype_1_1reactionparameters.html#a035ce3cb6900e72aa49c11b60ef6717f',1,'derivedtype::reactionparameters::reactants()']]],
  ['reactionlist',['reactionlist',['../structderivedtype_1_1cascade.html#afc95857b6abe4ff27b3127317767df56',1,'derivedtype::cascade::reactionlist()'],['../namespacemod__srscd__constants.html#a35b3f2ae88a10da0bf9b70cfcb833e71',1,'mod_srscd_constants::reactionlist()']]],
  ['reactionradius',['reactionradius',['../namespacemod__srscd__constants.html#a919e647e6614d2e2db714c218721bd86',1,'mod_srscd_constants']]],
  ['reactionrate',['reactionrate',['../structderivedtype_1_1reaction.html#af284771f2a24a568d1f9c8c53c6ceb01',1,'derivedtype::reaction']]],
  ['recombinationcoeff',['recombinationcoeff',['../namespacemod__srscd__constants.html#a4704dd5b67a90e43a28d65adc1d346e2',1,'mod_srscd_constants']]],
  ['restartfilename',['restartfilename',['../namespacemod__srscd__constants.html#ab080286bf223c7beb507a5ce90bed1ee',1,'mod_srscd_constants']]]
];
