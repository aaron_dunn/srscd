var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvz",
  1: "bcdmpr",
  2: "dmr",
  3: "cdfikmprs",
  4: "abcdfgiorstu",
  5: "abcdefghiklmnoprstvz",
  6: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

