! $Header: /home/CVS//srscd/tools/MeshGenerator/MeshGen.f90,v 1.2 2015/02/17 23:16:33 rdingre Exp $
!***************************************************************************************************
!
! This is the mesh generator program that will output mesh text files that are
! usable in SRSCD_par. The goal of this program is to have an easy and quick way to create uniform
! meshes for SRSCD without changing the (more demanding) inputs in SRSCD.
!
! Inputs: Volume element length (uniform volume elements only), meshType (periodic or free surfaces),
! 		number of elements in x, y, and z-directions.
! 
! Output: Text file that can be read into SRSCD_par with mesh. Each element's center coordinates 
!		as well as the global max/min coordinates in the x, y, and z directions will be given.
!		All materials will be of type 1.
!
!***************************************************************************************************

program MeshGen
implicit none

integer numx, numy, numz, i, j, k
double precision length
character(20) meshType, temp, filename
logical flag

!Input file where information on uniform cubic mesh is stored
open(81, file='MeshGenInput.txt',action='read', status='old')

!Part 1: Read in parameters from input file

flag=.FALSE.

do 10 while(flag .eqv. .FALSE.)

	read(81,*) temp
	
	if(temp=='meshType') then
		read(81,*) meshType
	else if(temp=='length') then
		read(81,*) length
	else if(temp=='numx') then
		read(81,*) numx
	else if(temp=='numy') then
		read(81,*) numy
	else if(temp=='numz') then
		read(81,*) numz
	else if(temp=='filename') then
		read(81,*) filename
	else if(temp=='end') then
		flag=.TRUE.
	endif
	
10 continue

!Part 2: Write output file

open(82, file=filename, action='write', status='unknown')

!First put in comment section

write(82,*) '!*************************************************************************************'
write(82,*) '!This file contains a UNIFORM cubic mesh input for the parallel SRSCD'
write(82,*) '!code. The mesh will be read in via coordinates (x,y,z) of each element center'
write(82,*) '!as well as the material number of each element (eg. material 1=Fe, 2=Cu, etc)'
write(82,*) '!'
write(82,*) '!The first values that will be read in are the number of elements in each direction'
write(82,*) '!(x,y,z).'
write(82,*) '!'
write(82,*) '!*************************************************************************************'
write(82,*)

!Next specify meshType

write(82,*) '!Specify mesh type (currently two types=periodic and freeSurfaces)'
write(82,*) 'meshType'
write(82,*) meshType
write(82,*)

!Next specify uniform cubic mesh element length

write(82,*) '!Specify element lengths (nm)'
write(82,*) 'length'
write(82,*) length
write(82,*)

!Next specify the min and max of each coordinate

write(82,*) '!Specify max and min of each coordinate (nm)'
write(82,*) '!NOTE: the actual max, min will be max, min +/- length/2 because these are assumed'
write(82,*) '!to be the center of the elements and not the edges'
write(82,*) 'xminmax' 
write(82,*) length/2d0, length/2d0+(numx-1)*length
write(82,*) 'yminmax'	
write(82,*) length/2d0, length/2d0+(numy-1)*length
write(82,*) 'zminmax'	
write(82,*) length/2d0, length/2d0+(numz-1)*length
write(82,*)

!Next specify numx, numy, numz

write(82,*) '!Specify number of elements in each direction'
write(82,*) 'numx'	
write(82,*) numx
write(82,*) 'numy'
write(82,*) numy
write(82,*) 'numz'	
write(82,*) numz
write(82,*)

!Next create a list of coordinates of centers of volume elements

write(82,*) '!Coordinates of element centers (x, y, z, elementType) (nm)'
write(82,*) '!NOTE: these must be ordered in the same way as the connectivity matrix (loop x, then y, then z)'
write(82,*) 'elements			!tells computer that the following values are elements'

do 11 i=1,numz
	do 12 j=1,numy
		do 13 k=1,numx
			write(82,*) (k-1)*length+length/2d0, (j-1)*length+length/2d0, (i-1)*length+length/2d0, 1
		13 continue
		write(82,*)
	12 continue
11 continue

end program
