! $Header: /home/CVS//srscd/src/Misc_functions.f90,v 1.5 2015/04/02 15:44:44 aydunn Exp $
!***************************************************************************************************
!> Subroutine factorial
!! Factorial function returns n!
!! 
!! Inputs: n (integer)
!! Outputs: factorial (integer)
!!
!! This is limited to numbers n<17 for computer feasibility.
!***************************************************************************************************

integer function factorial(n)
implicit none

integer n, i, temp

if(n .GE. 17) then
	write(*,*) 'error factorial too large'
endif

temp=1
do 10 i=1,n
	temp=temp*i
	!write(*,*) 'temp', temp
10 continue
factorial=temp

end function

!***************************************************************************************************
!> Subroutine binomial
!! Binomial function returns n choose k
!! 
!! Inputs: n, k (integers)
!! Outputs: Binomial (integer)
!***************************************************************************************************

integer function binomial(n,k)
implicit none

integer n, k, factorial

!write(*,*) 'binomial', n, k
!write(*,*) 'factorials', factorial(n), factorial(k), factorial(n-k)
!read(*,*) 
binomial=factorial(n)/(factorial(k)*factorial(n-k))

end function

!***************************************************************************************************
!
!> function TotalRateCheck() - checks if our total rate still matches the actual total rate
!!
!! This function is used as a diagnostic tool, it calcuates the total rate of all reactions in the 
!! system and compares it to totalRate, in order to test whether totalRate is being properly updated.
!!
!! Inputs: none
!! Output: total reaction rate (sum of all reaction rates)
!
!***************************************************************************************************

double precision function TotalRateCheck()
use mod_srscd_constants
use DerivedType
implicit none

integer cell, i, j, k
double precision rate
type(reaction), pointer :: reactionCurrent
type(cascade), pointer :: cascadeCurrent

CascadeCurrent=>ActiveCascades

rate=0d0

!Compute total rate of all reactions in the coarse mesh
do 10 i=1,numCells
	reactionCurrent=>reactionList(i)
	do 11 while(associated(reactionCurrent))
		rate=rate+reactionCurrent%reactionRate
		reactionCurrent=>reactionCurrent%next
	11 continue
10 continue

!Compute total rate of all reactions in the fine meshes by going through active cascades
do 12 while(associated(CascadeCurrent))
	do 13 i=1,numCellsCascade
		reactionCurrent=>CascadeCurrent%reactionList(i)
		do 14 while(associated(reactionCurrent))
			rate=rate+reactionCurrent%reactionRate
			reactionCurrent=>reactionCurrent%next
		14 continue
	13 continue
	CascadeCurrent=>CascadeCurrent%next
12 continue

!Compare rate to totalRate and return error if different
!if(myProc%taskid==MASTER) then
!	if (rate==totalRate) then
!		write(*,*) 'TotalRate correct', totalRate
!	else
!		write(*,*) 'TotalRate incorrect ', 'totalRate=', totalRate, 'actual total rate=', rate
!	endif
!endif

TotalRateCheck=rate

end function
