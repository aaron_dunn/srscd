! $Header: /home/CVS//srscd/src/Deallocate_Lists.f90,v 1.3 2015/04/02 15:44:44 aydunn Exp $
!***********************************************************************
!
!> Subroutine deallocateBoundaryDefectList() - deallocates boundary defect lists
!!
!! This subroutine deallocates all the defects in the boundary mesh at the
!! end of the simulation
!!
!! Inputs: none
!! Outputs: none
!! Action: goes through boundary mesh and deallocates all defects
!
!***********************************************************************

subroutine deallocateBoundaryDefectList()
use DerivedType
use mod_srscd_constants
implicit none

integer cell, dir, k, i
type(defect), pointer :: defectCurrent, defectPrev

do 10 cell=1,numCells
	do 11 dir=1,6
		do 12 k=1,myMesh(cell)%numNeighbors(dir)
			if(myMesh(cell)%neighborProcs(dir,k) .NE. myProc%taskid .AND. &
				myMesh(cell)%neighborProcs(dir,k) .NE. -1) then !neighbor cell is in different proc, not free surface (taskid=-1)
				
				if(myMesh(cell)%neighborProcs(dir,k) .NE. myProc%procNeighbor(dir)) then
					write(*,*) 'error neighbor not correct during boundary mesh initialization'
				else
					!deallocate this boundary element:
					!1) Peruse through defect list and deallocate all defects
					!2) Deallocate entire list
					
					defectCurrent=>myBoundary(dir,myMesh(cell)%neighbors(dir,k))%defectList%next
					
					do 13 while(associated(defectCurrent))
						defectPrev=>defectCurrent
						defectCurrent=>defectCurrent%next
						if(allocated(defectPrev%defectType)) then
							deallocate(DefectPrev%defectType)
						endif
						deallocate(defectPrev)
					13 continue

				endif
			endif
		12 continue
	11 continue
10 continue

end subroutine


!***********************************************************************
!
!> Subroutine deallocateCascadeList() - deallocates all stored cascade data (read in from file) in cascade list
!!
!! This subroutine deallocates all the cascades in the cascade list at the
!! end of the simulation.
!!
!! Inputs: none
!! Outputs: none
!! Action: goes through each cascade list and deallocates all cascades
!! (note: these are the lists used as inputs, not the fine mesh defects)
!
!***********************************************************************

Subroutine deallocateCascadeList()
use DerivedType
use mod_srscd_constants
implicit none

type(cascadeEvent), pointer :: CascadeTemp, CascadePrev
type(cascadeDefect), pointer :: DefectTemp, DefectPrev

CascadeTemp=>CascadeList

do 10 while(associated(CascadeTemp))

	DefectTemp=>CascadeTemp%listOfDefects
	
	do 11 while(associated(DefectTemp))
		
		DefectPrev=>DefectTemp
		DefectTemp=>DefectTemp%next
		
		if(allocated(defectPrev%defectType)) then
			deallocate(defectPrev%defectType)
		endif
		
		deallocate(defectPrev)
	
	11 continue
	
	CascadePrev=>CascadeTemp
	CascadeTemp=>CascadeTemp%nextCascade
	
	deallocate(CascadePrev)

10 continue

end subroutine

!***********************************************************************
!
!> Subroutine deallocateMaterialInput() - deallocates material input data (binding and migration energies, etc)
!!
!! This subroutine deallocates global variables used for calculating
!! allowed reactions:
!!
!!type(diffusionFunction), allocatable :: DiffFunc(:)
!!
!!type(diffusionSingle), allocatable :: DiffSingle(:)
!!
!!type(bindingSingle), allocatable :: BindSingle(:)
!!
!!type(bindingFunction), allocatable :: BindFunc(:)
!!
!!type(reactionParameters), allocatable :: DissocReactions(:), DiffReactions(:), SinkReactions(:)
!!
!!type(reactionParameters), allocatable :: ImpurityReactions(:), ClusterReactions(:), ImplantReactions(:)	
!
!***********************************************************************


subroutine deallocateMaterialInput()
use DerivedType
use mod_srscd_constants
implicit none

integer i

do 10 i=1,numFuncDiff
	deallocate(DiffFunc(i)%defectType)
	deallocate(DiffFunc(i)%min)
	deallocate(DiffFunc(i)%max)
	deallocate(DiffFunc(i)%parameters)
10 continue

if(allocated(DiffFunc)) deallocate(DiffFunc)

do 11 i=1,numSingleDiff
	deallocate(DiffSingle(i)%defectType)
11 continue

if(allocated(DiffSingle)) deallocate(DiffSingle)

do 12 i=1,numFuncBind
	deallocate(BindFunc(i)%defectType)
	deallocate(BindFunc(i)%product)
	deallocate(BindFunc(i)%min)
	deallocate(BindFunc(i)%max)
	deallocate(BindFunc(i)%parameters)
12 continue

if(allocated(BindFunc)) deallocate(BindFunc)

do 13 i=1,numSingleBind
	deallocate(BindSingle(i)%defectType)
	deallocate(BindSingle(i)%product)
13 continue

if(allocated(BindSingle)) deallocate(BindSingle)

do 14 i=1,numDissocReac
	deallocate(DissocReactions(i)%reactants)
	deallocate(DissocReactions(i)%products)
	deallocate(DissocReactions(i)%min)
	deallocate(DissocReactions(i)%max)
14 continue

if(allocated(DissocReactions)) deallocate(DissocReactions)

do 15 i=1,numDiffReac
	deallocate(DiffReactions(i)%reactants)
	deallocate(DiffReactions(i)%products)
	deallocate(DiffReactions(i)%min)
	deallocate(DiffReactions(i)%max)
15 continue

if(allocated(DiffReactions)) deallocate(DiffReactions)

do 16 i=1,numSinkReac
	deallocate(SinkReactions(i)%reactants)
	!deallocate(SinkReactions(i)%products)
	deallocate(SinkReactions(i)%min)
	deallocate(SinkReactions(i)%max)
16 continue

if(allocated(SinkReactions)) deallocate(SinkReactions)

do 17 i=1,numImpurityReac
	deallocate(ImpurityReactions(i)%reactants)
	deallocate(ImpurityReactions(i)%products)
	deallocate(ImpurityReactions(i)%min)
	deallocate(ImpurityReactions(i)%max)
17 continue

if(allocated(ImpurityReactions)) deallocate(ImpurityReactions)

do 18 i=1,numClusterReac
	deallocate(ClusterReactions(i)%reactants)
	deallocate(ClusterReactions(i)%products)
	deallocate(ClusterReactions(i)%min)
	deallocate(ClusterReactions(i)%max)
18 continue

if(allocated(ClusterReactions)) deallocate(ClusterReactions)

do 19 i=1,numImplantReac
!	deallocate(ImplantReactions(i)%reactants)
!	deallocate(ImplantReactions(i)%products)
!	deallocate(ImplantReactions(i)%min)
!	deallocate(ImplantReactions(i)%max)
19 continue

if(allocated(ImplantReactions)) deallocate(ImplantReactions)

end subroutine

!***********************************************************************
!
!> Subroutine deallocateDefectList() - deallocates defects in the coarse mesh
!!
!! This subroutine deallocates all the defects in the coarse mesh at the
!! end of the simulation.
!!
!! Inputs: none
!! Outputs: none
!! Action: goes through each defect list for each volume element and 
!! deallocates all defects
!
!***********************************************************************

subroutine deallocateDefectList()
use DerivedType
use mod_srscd_constants
implicit none

type(defect), pointer :: defectCurrent, defectPrev
integer cell, i, j

do 10 cell=1,numCells
	defectCurrent=>DefectList(cell)%next
	
	do 11 while(associated(defectCurrent))
	
		defectPrev=>defectCurrent
		defectCurrent=>defectCurrent%next
		
		if(allocated(defectPrev%defectType)) then
			deallocate(defectPrev%defectType)
		endif
		
		deallocate(defectPrev)
	
	11 continue

10 continue

do 12 cell=1,numCells
	defectCurrent=>defectList(cell)
	
	if(allocated(defectCurrent%defectType)) then
		deallocate(defectCurrent%defectType)
	endif
12 continue

deallocate(DefectList)

end subroutine

!***********************************************************************
!
!> Subroutine deallocateReactionList() - deallocates reactions in the coarse mesh
!!
!! This subroutine deallocates all the reactions in the coarse mesh at the
!! end of the simulation.
!!
!! Inputs: none
!! Outputs: none
!! Action: goes through each reaction list for each volume element and 
!! deallocates all reactions
!
!***********************************************************************

subroutine deallocateReactionList()
use DerivedType
use mod_srscd_constants
implicit none

type(reaction), pointer :: reactionCurrent, reactionPrev
integer cell, i, j

do 10 cell=1,numCells
	reactionCurrent=>reactionList(cell)%next
	
	do 11 while(associated(reactionCurrent))
	
		reactionPrev=>reactionCurrent
		reactionCurrent=>reactionCurrent%next
		
		if(allocated(reactionPrev%reactants)) then
			deallocate(reactionPrev%reactants)
		endif
		
		if(allocated(reactionPrev%products)) then
			deallocate(reactionPrev%products)
		endif
		
		if(allocated(reactionPrev%cellNumber)) then
			deallocate(reactionPrev%cellNumber)
		endif
		
		if(allocated(reactionPrev%taskid)) then
			deallocate(reactionPrev%taskid)
		endif
		
		deallocate(reactionPrev)
	
	11 continue

10 continue

do 12 cell=1,numCells

	reactionCurrent=>reactionList(cell)
	
	if(allocated(reactionCurrent%reactants)) then
		deallocate(reactionCurrent%reactants)
	endif
	
	if(allocated(reactionCurrent%products)) then
		deallocate(reactionCurrent%products)
	endif
	
	if(allocated(reactionCurrent%cellNumber)) then
		deallocate(reactionCurrent%cellNumber)
	endif
	
	if(allocated(reactionCurrent%taskid)) then
		deallocate(reactionCurrent%taskid)
	endif
	
12 continue

deallocate(reactionList)

end subroutine
