! $Header: /home/CVS//srscd/src/Postprocessing_srscd.f90,v 1.3 2015/04/02 15:44:45 aydunn Exp $
!***************************************************************************************************
!>subroutine outputDefects - outputs raw data for defect populations in various volume elements
!!
!!Outputs into file: rawdat.out. These contain the complete
!!defect populations per volume element.
!!
!!Compiles data from local as well as global processors.
!***************************************************************************************************

subroutine outputDefects()
use mod_srscd_constants
use DerivedType
implicit none
include 'mpif.h'

integer buffer, tag, i, j, k, status(MPI_STATUS_SIZE), numCellsRecv, numDefectsRecv
integer defectTypeRecv(numSpecies), cellNumberRecv, numRecv, defectCount
type(defect), pointer :: defectCurrent
double precision coordinatesRecv(3)

tag=0

if(myProc%taskid==MASTER) then
	!first calculate DPA using MPI_ALLREDUCE
	
	write(82,*) 'dpa', DPA
	
	!Write defects in processor 0 to file
	write(82,*) 'processor', myProc%taskid
	do 12 i=1,numCells
		defectCurrent=>defectList(i)%next
		write(82,*) 'coordinates', myMesh(i)%coordinates
		do 13 while(associated(defectCurrent))
			write(82,*) defectCurrent%defectType, 'cell', defectCurrent%cellNumber, 'num', defectCurrent%num
			defectCurrent=>defectCurrent%next
		13 continue
	12 continue
	
	!Other processors will send information about defects contained in their mesh. This processor 
	!must output all information to data file (can't do it from other processors). Information should
	!be output in the same format for the master and slave processors.
	do 14 i=1,myProc%numtasks-1
		write(82,*) 'processor', i
		call MPI_RECV(numCellsRecv,1,MPI_INTEGER,i,99,MPI_COMM_WORLD,status,ierr)
		
		do 15 j=1,numCellsRecv
			call MPI_RECV(coordinatesRecv,3,MPI_DOUBLE_PRECISION,i,100,MPI_COMM_WORLD,status,ierr)
			write(82,*) 'coordinates', coordinatesRecv
			
			call MPI_RECV(numDefectsRecv,1,MPI_INTEGER,i,101,MPI_COMM_WORLD,status,ierr)
			do 16 k=1,numDefectsRecv
				call MPI_RECV(defectTypeRecv,numSpecies,MPI_INTEGER,i,102,MPI_COMM_WORLD,status,ierr)
				call MPI_RECV(cellNumberRecv,1,MPI_INTEGER,i,103,MPI_COMM_WORLD,status,ierr)
				call MPI_RECV(numRecv,1,MPI_INTEGER,i,104,MPI_COMM_WORLD,status,ierr)
				write(82,*) defectTypeRecv, 'cell', cellNumberRecv,'num',numRecv
			16 continue
			
			!send a signal telling the other processor to go on to the next cell
			call MPI_SEND(tag,1,MPI_INTEGER,i,105,MPI_COMM_WORLD,ierr)
		15 continue
	14 continue

else
		
	call MPI_SEND(numCells, 1, MPI_INTEGER, MASTER, 99, MPI_COMM_WORLD, ierr)
	
	do 17 i=1,numCells
		call MPI_SEND(myMesh(i)%coordinates, 3, MPI_DOUBLE_PRECISION, MASTER, 100, MPI_COMM_WORLD, ierr)
		
		!calculate the number of defect types in cell i and send to MASTER.
		defectCount=0
		defectCurrent=>defectList(i)%next
		do 18 while(associated(defectCurrent))
			defectCount=defectCount+1
			defectCurrent=>defectCurrent%next
		18 continue
		
		!send number of defects
		call MPI_SEND(defectCount,1,MPI_INTEGER,MASTER,101,MPI_COMM_WORLD, ierr)
		
		!send actual defect data (loop through defects a second time)
		defectCurrent=>defectList(i)%next
		do 19 while(Associated(defectCurrent))
			call MPI_SEND(defectCurrent%defectType,numSpecies,MPI_INTEGER,MASTER,102,MPI_COMM_WORLD,ierr)
			call MPI_SEND(defectCurrent%cellNumber,1,MPI_INTEGER,MASTER,103,MPI_COMM_WORLD,ierr)
			call MPI_SEND(defectCurrent%num,1,MPI_INTEGER,MASTER,104,MPI_COMM_WORLD,ierr)
			defectCurrent=>defectCurrent%next
		19 continue
		
		!This just pauses the sending until the master has recieved all of the above information
		call MPI_RECV(buffer,1,MPI_INTEGER,MASTER,105,MPI_COMM_WORLD,status,ierr)
	17 continue
endif

end subroutine

!***********************************************************************
!
!> Subroutine outputDefectsTotal() - outputs the total defects and post processing for the entire volume
!!
!! Outputs a list of all defects in system (defect type, num), regardless
!! of volume element. Compiles such a list using message passing between
!! processors. Used to find total defect populations in simulation volume,
!! not spatial distribution of defects.
!!
!! This subroutine will also automatically output the concentration of
!! vacancies, SIAs, and SIAs of size larger than 16, 20, and 24 defects.
!! (Corresponding to diameters 0.9 nm, 1.0 nm, 1.1 nm)
!
!***********************************************************************

subroutine outputDefectsTotal(elapsedTime, step)
use DerivedType
use mod_srscd_constants
implicit none

include 'mpif.h'

integer buffer, tag, i, j, k, status(MPI_STATUS_SIZE), numDefectsRecv, numDefectsSend
integer products(numSpecies)
integer defectTypeRecv(numSpecies), cellNumberRecv, numRecv, defectCount, same
type(defect), pointer :: defectCurrent, defectPrevList, defectCurrentList, outputDefectList
double precision defectsRecv(numSpecies+1), defectsSend(numSpecies+1)

!Simulation variables passed by main program
double precision elapsedTime
integer step

!variables for computation statistics
integer reactionsCoarse, reactionsFine

!total number of annihilation reactions
integer numAnnihilateTotal

!Variables for defect counting and concentration
integer VNum, SIANum, LoopSize(3), totalVac, totalSIA, HeNum, totalHe
integer totalVoid, totalLoop, VoidNum
double precision systemVol

interface
	subroutine findDefectInList(defectCurrent, defectPrev, products)
	use mod_srscd_constants
	type(defect), pointer :: defectCurrent, defectPrev
	integer products(numSpecies)
	end subroutine
end interface

tag=0

!initialize outputDefectList
allocate(outputDefectList)
allocate(outputDefectList%defectType(numSpecies))
nullify(outputDefectList%next)
do 1 i=1,numSpecies
	outputDefectList%defectType(i)=0
1 continue
outputDefectList%cellNumber=0
outputDefectList%num=0

call MPI_ALLREDUCE(numAnnihilate,numAnnihilateTotal,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ierr)

if(myProc%taskid==MASTER) then
	!first calculate DPA using MPI_ALLREDUCE
	
	write(83,*) 'dpa', DPA
	
	!Create list of defects in processor 0
	do 12 i=1,numCells
		defectCurrent=>defectList(i)%next
		do 13 while(associated(defectCurrent))
			
			nullify(defectPrevList)
			
			defectCurrentList=>outputDefectList
			
			call findDefectInList(defectCurrentList, defectPrevList, defectCurrent%defectType)
			
			!Next update defects
			if(associated(defectCurrentList)) then !if we aren't at the end of the list
				same=0
				
				do 709 j=1,numSpecies
					if(defectCurrentList%defectType(j)==defectCurrent%defectType(j)) then
						same=same+1
					endif
				709 continue
				
				if(same==numSpecies) then	
				
					!if the defect is already present in the list
				
					defectCurrentList%num=defectCurrentList%num+defectCurrent%num
				
				else		
					
					!if the defect is to be inserted in the list
					
					nullify(defectPrevList%next)
					allocate(defectPrevList%next)
					nullify(defectPrevList%next%next)
					defectPrevList=>defectPrevList%next
					allocate(defectPrevList%defectType(numSpecies))
					defectPrevList%cellNumber=0	!no need for cell numbers in outputDefectList
					defectPrevList%num=defectCurrent%num
					
					do 710 j=1,numSpecies
						defectPrevList%defectType(j)=defectCurrent%defectType(j)
					710 continue
					
					!if inserted defect is in the middle of the list, point it to the next item in the list
					
					defectPrevList%next=>defectCurrentList
				endif
			else if(associated(defectPrevList)) then			
				
				!add a defect to the end of the list
				
				nullify(defectPrevList%next)
				allocate(defectPrevList%next)
				nullify(defectPrevList%next%next)
				defectPrevList=>defectPrevList%next
				allocate(defectPrevList%defectType(numSpecies))
				defectPrevList%cellNumber=0 !no need for cell numbers in outputDefectList
				defectPrevList%num=defectCurrent%num
				
				do 711 j=1,numSpecies
					defectPrevList%defectType(j)=defectCurrent%defectType(j)
				711 continue
				
			else
				
				write(*,*) 'error tried to insert defect at beginning of output defect list'

			endif
			
			defectCurrent=>defectCurrent%next
		13 continue
	12 continue
	
	!Other processors will send information about defects contained in their mesh. This processor 
	!must output all information to data file (can't do it from other processors). Information should
	!be output in the same format for the master and slave processors.
	
	do 14 i=1,myProc%numtasks-1
	
		call MPI_RECV(numDefectsRecv,1,MPI_INTEGER,i,399,MPI_COMM_WORLD,status,ierr)
		
!		write(*,*) 'recieving ', numDefectsRecv, 'defects'
		
		do 15 j=1,numDefectsRecv
			call MPI_RECV(defectsRecv,numSpecies+1,MPI_DOUBLE_PRECISION,i,400,MPI_COMM_WORLD,status,ierr)
!			write(*,*) 'recieved defect ', j
			
			do 16 k=1,numSpecies
				products(k)=defectsRecv(k)
			16 continue
			
			nullify(defectPrevList)
			
			defectCurrentList=>outputDefectList
			
			call findDefectInList(defectCurrentList, defectPrevList, products)
			
			!Next update defects
			if(associated(defectCurrentList)) then !if we aren't at the end of the list
				same=0
				
				do 809 k=1,numSpecies
					if(defectCurrentList%defectType(k)==products(k)) then
						same=same+1
					endif
				809 continue
				
				if(same==numSpecies) then	
				
					!if the defect is already present in the list
				
					defectCurrentList%num=defectCurrentList%num+defectsRecv(numSpecies+1)
				
				else		
					
					!if the defect is to be inserted in the list
					
					nullify(defectPrevList%next)
					allocate(defectPrevList%next)
					nullify(defectPrevList%next%next)
					defectPrevList=>defectPrevList%next
					allocate(defectPrevList%defectType(numSpecies))
					defectPrevList%cellNumber=0	!no need for cell numbers in outputDefectList
					defectPrevList%num=defectsRecv(numSpecies+1)
					
					do 810 k=1,numSpecies
						defectPrevList%defectType(k)=products(k)
					810 continue
					
					!if inserted defect is in the middle of the list, point it to the next item in the list
					
					defectPrevList%next=>defectCurrentList
				endif
			else if(associated(defectPrevList)) then			
				
				!add a defect to the end of the list
				
				nullify(defectPrevList%next)
				allocate(defectPrevList%next)
				nullify(defectPrevList%next%next)
				defectPrevList=>defectPrevList%next
				allocate(defectPrevList%defectType(numSpecies))
				defectPrevList%cellNumber=0 !no need for cell numbers in outputDefectList
				defectPrevList%num=defectsRecv(numSpecies+1)
				
				do 811 k=1,numSpecies
					defectPrevList%defectType(k)=products(k)
				811 continue
				
			else
				
				write(*,*) 'error tried to insert defect at beginning of output defect list'

			endif
			
			!Signal to other processor to send the next defect
			call MPI_SEND(tag,1,MPI_INTEGER, i, 405,MPI_COMM_WORLD, ierr)
		15 continue
		
	14 continue
	
	!Output defect list
!	write(*,*) 'Defects ', 'num'
	write(83,*) 'Defects ', 'num'
	defectCurrentList=>outputDefectList
	
	!Initialize Defect counters
	HeNum=0
	VNum=0
	SIANum=0
	totalHe=0
	totalVac=0
	totalSIA=0
	totalVoid=0
	totalLoop=0
	
	do 90 i=1,3
		LoopSize(i)=0
	90 continue
	VoidNum=0
	
	do 20 while(associated(defectCurrentList))
		
		!Compile statistics for vacancy and SIA concentrations
		if(defectCurrentList%defectType(1) .NE. 0) then
			
			HeNum=HeNum+defectCurrentList%num
			
			totalHe=totalHe+defectCurrentList%defectType(1)*defectCurrentList%num
		
		endif
		
		if(defectCurrentList%defectType(2) .NE. 0) then
		
			VNum=VNum+defectCurrentList%num
			
			totalVac=totalVac+defectCurrentList%defectType(2)*defectCurrentList%num
			
			if(defectCurrentList%defectType(2) .GE. 45) then
				VoidNum=VoidNum+defectCurrentList%num
				totalVoid = totalVoid + defectCurrentList%defectType(2)*defectCurrentList%num
			endif
		endif
		
		if(defectCurrentList%defectType(4) .NE. 0 .OR. defectCurrentList%defectType(3) .NE. 0) then
		
			SIANum=SIANum+defectCurrentList%num
			
			totalSIA=totalSIA+defectCurrentList%defectType(4)*defectCurrentList%num
			totalSIA=totalSIA+defectCurrentList%defectType(3)*defectCurrentList%num
		
			if(defectCurrentList%defectType(4) .GE. 16) then
				LoopSize(1)=LoopSize(1)+defectCurrentList%num
			endif
			
			if(defectCurrentList%defectType(4) .GE. 20) then
				LoopSize(2)=LoopSize(2)+defectCurrentList%num
				totalLoop = totalLoop + defectCurrentList%defectType(4)*defectCurrentList%num
			endif
			
			if(defectCurrentList%defectType(4) .GE. 24) then
				LoopSize(3)=LoopSize(3)+defectCurrentList%num
			endif
		endif
	
!		write(*,*) defectCurrentList%defectType, defectCurrentList%num
		write(83,*) defectCurrentList%defectType, defectCurrentList%num
		defectCurrentList=>defectCurrentList%next
	20 continue
	
	systemVol=(myProc%globalCoord(2)-myProc%globalCoord(1))*&
			  (myProc%globalCoord(4)-myProc%globalCoord(3))*&
			  (myProc%globalCoord(6)-myProc%globalCoord(5))*1d-27
	
	write(83,*) 'HeNum', HeNum, 'Conc', dble(HeNum)/systemVol
	write(83,*) 'VNum', VNum, 'Conc', dble(VNum)/systemVol
	write(83,*)	'Voids', VoidNum, 'Conc', dble(VoidNum)/systemVol
	write(83,*) 'SIANum', SIANum, 'Conc', dble(SIANum)/systemVol
!	write(83,*) 'SIA_small',LoopSize(1), 'Conc', dble(LoopSize(1))/systemVol
	write(83,*) 'Loops', LoopSize(2), 'Conc', dble(LoopSize(2))/systemVol
!	write(83,*) 'SIA_large', LoopSize(3), 'Conc', dble(LoopSize(3))/systemVol
	
	write(84,*) 'HeNum', HeNum, 'Conc', dble(HeNum)/systemVol
	write(84,*) 'VNum', VNum, 'Conc', dble(VNum)/systemVol
	write(84,*)	'Voids', VoidNum, 'Conc', dble(VoidNum)/systemVol
	write(84,*) 'SIANum', SIANum, 'Conc', dble(SIANum)/systemVol
!	write(84,*) 'SIA_small',LoopSize(1), 'Conc', dble(LoopSize(1))/systemVol
	write(84,*) 'Loops', LoopSize(2), 'Conc', dble(LoopSize(2))/systemVol
!	write(84,*) 'SIA_large', LoopSize(3), 'Conc', dble(LoopSize(3))/systemVol
	
	!*******************************************************************
	!Post processing: calculate relevant information (hard coded) to 
	!output into postprocessing.txt
	!*******************************************************************
	
	!Average vacancy size, average SIA size
	write(84,*) 'AverageHeSize', dble(totalHe)/dble(HeNum)
	write(84,*) 'AverageVSize', dble(totalVac)/dble(VNum)
	write(84,*) 'AverageSIASize', dble(totalSIA)/dble(SIANum)
	write(84,*) 'AverageVoidSize', dble(totalVoid)/dble(VoidNum)
	write(84,*) 'AverageLoopSize', dble(totalLoop)/dble(LoopSize(2))
	
	!Percent vacancies retained/annihilated
	write(84,*) 'PercentHeRetained', dble(totalHe)/dble(numHeImplantTotal)
	write(84,*) 'PercentVRetained', dble(totalVac)/(dble(numDisplacedAtoms)*dble(totalImplantEvents))
	write(84,*) 'PercentVAnnihilated', dble(numAnnihilateTotal)/(dble(numDisplacedAtoms)*dble(totalImplantEvents))
	
	!Computation statistics: number of reactions in coarse/fine mesh, average timestep
	call countReactionsCoarse(reactionsCoarse)
	call countReactionsFine(reactionsFine)
	write(84,*) 'NumReactionsCoarse', reactionsCoarse
	write(84,*) 'NumReactionsFine', reactionsFine
	write(84,*) 'AverageTimestep', elapsedTime/dble(step)
	
	!Leave blank line
	write(83,*)
	write(84,*)

else
	numDefectsSend=0
	
	!Create list of defects in processor 
	do 22 i=1,numCells
		defectCurrent=>defectList(i)%next
		do 23 while(associated(defectCurrent))
			
			nullify(defectPrevList)
			
			defectCurrentList=>outputDefectList
			
			call findDefectInList(defectCurrentList, defectPrevList, defectCurrent%defectType)
			
			!Next update defects
			if(associated(defectCurrentList)) then !if we aren't at the end of the list
				same=0
				
				do 909 j=1,numSpecies
					if(defectCurrentList%defectType(j)==defectCurrent%defectType(j)) then
						same=same+1
					endif
				909 continue
				
				if(same==numSpecies) then	
				
					!if the defect is already present in the list
				
					defectCurrentList%num=defectCurrentList%num+defectCurrent%num
				
				else		
					
					!if the defect is to be inserted in the list
					
					nullify(defectPrevList%next)
					allocate(defectPrevList%next)
					nullify(defectPrevList%next%next)
					defectPrevList=>defectPrevList%next
					allocate(defectPrevList%defectType(numSpecies))
					defectPrevList%cellNumber=0	!no need for cell numbers in outputDefectList
					defectPrevList%num=defectCurrent%num
					
					do 910 j=1,numSpecies
						defectPrevList%defectType(j)=defectCurrent%defectType(j)
					910 continue
					
					!if inserted defect is in the middle of the list, point it to the next item in the list
					
					defectPrevList%next=>defectCurrentList
					
					numDefectsSend=numDefectsSend+1
				endif
			else if(associated(defectPrevList)) then			
				
				!add a defect to the end of the list
				
				nullify(defectPrevList%next)
				allocate(defectPrevList%next)
				nullify(defectPrevList%next%next)
				defectPrevList=>defectPrevList%next
				allocate(defectPrevList%defectType(numSpecies))
				defectPrevList%cellNumber=0 !no need for cell numbers in outputDefectList
				defectPrevList%num=defectCurrent%num
				
				do 911 j=1,numSpecies
					defectPrevList%defectType(j)=defectCurrent%defectType(j)
				911 continue
				
				numDefectsSend=numDefectsSend+1
				
			else
				
				write(*,*) 'error tried to insert defect at beginning of output defect list'

			endif
			
			defectCurrent=>defectCurrent%next
		23 continue
	22 continue	
		
	call MPI_SEND(numDefectsSend, 1, MPI_INTEGER, MASTER, 399, MPI_COMM_WORLD, ierr)
	
	defectCurrentList=>outputDefectList%next
	i=0
	
	do 17 while(associated(defectCurrentList))
		i=i+1
	
		do 24 j=1,numSpecies
			defectsSend(j)=defectCurrentList%defectType(j)
		24 continue
		
		defectsSend(numSpecies+1)=defectCurrentList%num
		
		call MPI_SEND(defectsSend, numSpecies+1, MPI_DOUBLE_PRECISION, MASTER, 400, MPI_COMM_WORLD, ierr)
		
		!This just pauses the sending until the master has recieved all of the above information
		call MPI_RECV(buffer,1,MPI_INTEGER,MASTER,405,MPI_COMM_WORLD,status,ierr)
		
		defectCurrentList=>defectCurrentList%next
		
		if(i==numDefectsSend .AND. associated(defectCurrentList)) then
			write(*,*) 'error outputDefectList size does not match numDefectsSend'
		endif
		
!		write(*,*) 'sent defect', i
	17 continue
	
	call countReactionsCoarse(reactionsCoarse)
	call countReactionsFine(reactionsFine)
endif

!Deallocate memory

defectCurrentList=>outputDefectList
nullify(defectPrevList)

do 25 while(Associated(defectCurrentList))
	defectPrevList=>defectCurrentList
	defectCurrentList=>defectCurrentList%next
	
	if(allocated(defectPrevList%defectType)) deallocate(defectPrevList%defectType)
	
	deallocate(defectPrevList)
25 continue

nullify(defectCurrentList)
nullify(outputDefectList)

end subroutine

!***************************************************************************************************
!
!> Subroutine OutputDefectsProfile(sim) - outputs depth profile of defect concentrations
!!
!! This subroutine outputs a list of the z-coordinates in the given system as well as the total
!! concentration of vacancies, interstitials, and helium atoms at each depth. This is meant to 
!! be used by the Gnuplot script DepthProfile.p.
!!
!! Inputs: Defect and mesh information, simulation number (for filename)
!! Outputs: Text document with defect profile
!
!***************************************************************************************************

subroutine outputDefectsProfile(sim)
use DerivedType
use mod_srscd_constants
implicit none

include 'mpif.h'

integer, allocatable :: defectProfileArray(:,:)
integer numZ, numX, numY, zEntry, i, j, procID, tempArray(3), status(MPI_STATUS_SIZE), sim
type(defect), pointer :: defectCurrent
double precision zCoord
character*20 filename

!Create an array with z-coordinates given by the z-coordinates of the mesh elements

!First calculate the number of z-coordinates in the entire system

!Assuming uniform mesh (if nonuniform, we cannot use myMesh(1)%length as the length of every element
numZ=int(((myProc%globalCoord(6)-myProc%globalCoord(5))/myMesh(1)%length))
numX=int(((myProc%globalCoord(2)-myProc%globalCoord(1))/myMesh(1)%length))
numY=int(((myProc%globalCoord(4)-myProc%globalCoord(3))/myMesh(1)%length))

!Allocate and initialize array of defect numbers in each processor
allocate(defectProfileArray(numZ,numSpecies))
do 10 i=1,numZ
	do 11 j=1,numSpecies
		defectProfileArray(i,j)=0
	11 continue
10 continue

!Go through defects in every mesh element and add them to defectProfileArray
do 12 i=1,numCells
	
	!This tells us which index in defectProfileArray we should be adding to
	zEntry=int((myMesh(i)%coordinates(3)+myMesh(i)%length/2d0)/myMesh(i)%length)
	
	defectCurrent=>defectList(i)
	do 13 while(associated(defectCurrent))
		
		!Add the number of defects of each species to defectProfileArray
		do 14 j=1,numSpecies
			defectProfileArray(zEntry,j)=defectProfileArray(zEntry,j)+defectCurrent%defectType(j)*defectCurrent%num
		14 continue
		defectCurrent=>defectCurrent%next

	13 continue

12 continue
	
!Add up all defect numbers for each processor
if(myProc%taskid==MASTER) then

	do 16 procID=1,myProc%numTasks-1
		
		do 17 i=1,numZ

			call MPI_RECV(tempArray,numSpecies,MPI_INTEGER, procID, i*700, MPI_COMM_WORLD, status, ierr)
			
			!Add defects of neighboring procs to master processor defect array
			do 18 j=1,numSpecies
				defectProfileArray(i,j)=defectProfileArray(i,j)+tempArray(j)
			18 continue
		
		17 continue
	
	16 continue
	
	!Outputs defectProfileArray to a file

	filename(1:12)='DepthProfile'
	write(unit=filename(13:13), fmt='(I1)') sim
	filename(14:17)='.txt'
	
	open(99, file=filename, action='write', status='Unknown')
	
	write(99,*) 'ZCoord ', 'HeConc ', 'VConc ', 'SIAMobileConc ', 'SIASessileConc'
	
	do 19 i=1,numZ
		ZCoord=(i-1)*myMesh(1)%length+myMesh(1)%length/2d0
		
		write(99,*) ZCoord, (dble(defectProfileArray(i,j)/(numX*numY*((1d-9*myMesh(1)%length)**3d0))),&
			j=1,3)
	19 continue
	
	close(99)
	
else
	
	!Send local array to be added to master processor array
	do 15 i=1,numZ
		call MPI_SEND(defectProfileArray(i,:),numSpecies,MPI_INTEGER, MASTER, i*700, MPI_COMM_WORLD, ierr)
	15 continue
	
endif

end subroutine

!***********************************************************************
!
!> Subroutine outputDefectsXYZ - outputs xyz file of defect concentrations
!!
!! Outputs number of vacancies / SIAs per volume element in .XYZ file
!! (see wikipedia page for documentation on file format)
!!
!! Only outputs defects in the main mesh (ignores fine meshes present)
!
!***********************************************************************

subroutine outputDefectsXYZ()
use mod_srscd_constants
use DerivedType
implicit none
include 'mpif.h'

integer HeCount, VCount, SIACount, numPoints, cellCount
integer numCellsNeighbor
integer i, j
integer status(MPI_STATUS_SIZE)
double precision coords(3)
type(defect), pointer :: defectCurrent

if(myProc%taskid==MASTER) then
	
	cellCount=1	!for outputting cell number in .xyz file
	
	call MPI_ALLREDUCE(numCells, numPoints, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
	
	write(87,*) numPoints
	write(87,*) 'CellNumber ', 'x ', 'y ', 'z ', 'NumHe ', 'NumV ', 'NumSIA'
	
	!Write information in master processor
	
	do 10 i=1,numCells
		
		HeCount=0
		VCount=0
		SIACount=0
		
		!Count defects in cell i
		defectCurrent=>defectList(i)
		do 11 while(associated(defectCurrent))
			
			HeCount	 = HeCount+defectCurrent%defectType(1)*defectCurrent%num
			VCount	 = VCount+defectCurrent%defectType(2)*defectCurrent%num
			SIACount = SIACount+defectCurrent%defectType(3)*defectCurrent%num + &
					   defectCurrent%defectType(4)*defectCurrent%num
			
			defectCurrent=>defectCurrent%next
		
		11 continue
		
		write(87,59) cellCount, myMesh(i)%coordinates(1), myMesh(i)%coordinates(2), &
			& myMesh(i)%coordinates(3), HeCount, VCount, SIACount
			
		cellCount=cellCount+1
		
	10 continue
	
	!Recieve information from neighboring processors
	
	do 12 i=1,myProc%numTasks-1
	
		call MPI_RECV(numCellsNeighbor, 1, MPI_INTEGER, i, 567, MPI_COMM_WORLD, status, ierr)
		
		do 13 j=1,numCellsNeighbor
		
			call MPI_RECV(coords, 3, MPI_DOUBLE_PRECISION, i, 571, MPI_COMM_WORLD, status, ierr)
			call MPI_RECV(HeCount, 1, MPI_INTEGER, i, 568, MPI_COMM_WORLD, status, ierr)
			call MPI_RECV(VCount, 1, MPI_INTEGER, i, 569, MPI_COMM_WORLD, status, ierr)
			call MPI_RECV(SIACount, 1, MPI_INTEGER, i, 570, MPI_COMM_WORLD, status, ierr)
			
			write(87,59) cellCount, coords(1), coords(2), coords(3), &
				HeCount, VCount, SIACount
				
			cellCount=cellCount+1
			
		13 continue
		
	12 continue

else

	call MPI_ALLREDUCE(numCells, numPoints, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
	
	call MPI_SEND(numCells, 1, MPI_INTEGER, MASTER, 567, MPI_COMM_WORLD, ierr)
	
	do 14 i=1,numCells
	
		call MPI_SEND(myMesh(i)%coordinates, 3, MPI_DOUBLE_PRECISION, MASTER, 571, MPI_COMM_WORLD, ierr)
		
		HeCount=0
		VCount=0
		SIACount=0
		
		defectCurrent=>defectList(i)
		
		do 15 while(associated(defectCurrent))
			
			HeCount	 = HeCount+defectCurrent%defectType(1)*defectCurrent%num
			VCount	 = VCount+defectCurrent%defectType(2)*defectCurrent%num
			SIACount = SIACount+defectCurrent%defectType(3)*defectCurrent%num + &
					   defectCurrent%defectType(4)*defectCurrent%num
			
			defectCurrent=>defectCurrent%next
			
		15 continue
		
		call MPI_SEND(HeCount, 1, MPI_INTEGER, MASTER, 568, MPI_COMM_WORLD, ierr)
		call MPI_SEND(VCount, 1, MPI_INTEGER, MASTER, 569, MPI_COMM_WORLD, ierr)
		call MPI_SEND(SIACount, 1, MPI_INTEGER, MASTER, 570, MPI_COMM_WORLD, ierr)
	
	14 continue

endif

 59		format(i4,3(1x,e12.4),3(1x,i6))

end subroutine

!***********************************************************************
!
!> Subroutine outputDefectsVTK - outputs defect populations in vtk file format
!!
!! Outputs defects in a VTK formatted file (3D unstructured grid of linear cubes)
!
!***********************************************************************

subroutine outputDefectsVTK(fileNumber)
use mod_srscd_constants
use DerivedType
implicit none
include 'mpif.h'

integer fileNumber, numx, numy, numz
integer i,j,k
integer xindex, yindex, zindex
integer numV, numHe, numSIA
integer numCellsNeighbor, cellIndex(3)
integer status(MPI_STATUS_SIZE)

integer, allocatable :: DefectListVTK(:,:,:,:)

double precision xlength, ylength, zlength
type(defect), pointer :: defectCurrent
character(13) :: fileName

if(myProc%taskid==MASTER) then

!Step 1: Find the dimensions of the (global) mesh (assume cubic elements)

numx=(myProc%globalCoord(2)-myProc%globalCoord(1))/myMesh(1)%length
numy=(myProc%globalCoord(4)-myProc%globalCoord(3))/myMesh(1)%length
numz=(myProc%globalCoord(6)-myProc%globalCoord(5))/myMesh(1)%length

xlength=myMesh(1)%length
ylength=myMesh(1)%length
zlength=myMesh(1)%length

!Step 2: Allocate DefectListVTK to the same dimensions as global mesh

allocate(DefectListVTK(numx,numy,numz,3))

!Step 3: Fill in DefectListVTK

do 19 i=1,numCells
	
	!Step 3.1: identify the x,y,z index of this cell
	xindex=(myMesh(i)%coordinates(1)-myProc%globalCoord(1))/myMesh(1)%length+1
	yindex=(myMesh(i)%coordinates(2)-myProc%globalCoord(3))/myMesh(1)%length+1
	zindex=(myMesh(i)%coordinates(3)-myProc%globalCoord(5))/myMesh(1)%length+1
	
	!Step 3.2: count number of vacancies, interstitials, and He atoms in this cell
	defectCurrent=>defectList(i)
	
	numV=0
	numSIA=0
	numHe=0
	
	do 20 while(associated(defectCurrent))
		numV=numV+defectCurrent%defectType(2)*defectCurrent%num
		numHe=numHe+defectCurrent%defectType(1)*defectCurrent%num
		numSIA=numSIA+defectCurrent%defectType(3)*defectCurrent%num + &
			defectCurrent%defectType(4)*defectCurrent%num
		
		defectCurrent=>defectCurrent%next
	20 continue
	
	!Step 3.3: enter in data point in DefectListVTK
	DefectListVTK(xindex,yindex,zindex,1)=numHe
	DefectListVTK(xindex,yindex,zindex,2)=numV
	DefectListVTK(xindex,yindex,zindex,3)=numSIA
	
19 continue

!Step 3.4: recieve data from other processors and fill into DefectListVTK
do 21 i=1,myProc%numTasks-1
	
	call MPI_RECV(numCellsNeighbor, 1, MPI_INTEGER, i, 567, MPI_COMM_WORLD, status, ierr)
		
	do 22 j=1,numCellsNeighbor
	
		call MPI_RECV(cellIndex, 3, MPI_INTEGER, i, 571, MPI_COMM_WORLD, status, ierr)
		call MPI_RECV(numHe, 1, MPI_INTEGER, i, 568, MPI_COMM_WORLD, status, ierr)
		call MPI_RECV(numV, 1, MPI_INTEGER, i, 569, MPI_COMM_WORLD, status, ierr)
		call MPI_RECV(numSIA, 1, MPI_INTEGER, i, 570, MPI_COMM_WORLD, status, ierr)
		
		DefectListVTK(cellIndex(1),cellIndex(2),cellIndex(3),1)=numHe
		DefectListVTK(cellIndex(1),cellIndex(2),cellIndex(3),2)=numV
		DefectListVTK(cellIndex(1),cellIndex(2),cellIndex(3),3)=numSIA
				
	22 continue
	
21 continue

!Step 4: Ouptut data in VTK file format, taking data from DefectListVTK

fileName(1:6)='VTKout'
write(unit=fileName(7:9), fmt='(I3)') fileNumber
fileName(10:13)='.vtk'

open(88, file=fileName, status='Unknown')

write(88,'(a)') '# vtk DataFile Version 1.0'
write(88,'(a)') '3D Unstructured Grid of Linear Cubes'
write(88,'(a)') 'ASCII'
write(88,*)
write(88,'(a)') 'DATASET STRUCTURED_POINTS'
write(88,'(a)', advance='no') 'DIMENSIONS '
write(88,'(I4,I4,I4)') numx, numy, numz
write(88,'(a)', advance='no') 'ORIGIN '
write(88,'(I4,I4,I4)') 0, 0, 0
write(88,'(a)', advance='no') 'SPACING '
write(88,*) xlength, ylength, zlength
write(88,'(a)', advance='no') 'POINT_DATA '
write(88,'(I4)') numx*numy*numz

write(88,'(a,a,a,I4)') 'SCALARS ', 'Helium', ' float', 1
write(88,'(a,a)') 'LOOKUP_TABLE ', 'DEFAULT'

do 10 i=1,numx
do 11 j=1,numy
do 12 k=1,numz

	write(88,*) DefectListVTK(i,j,k,1)

12 continue
11 continue
10 continue

write(88,'(a,a,a,I4)') 'SCALARS ', 'Vacancies', ' float', 1
write(88,'(a,a)') 'LOOKUP_TABLE ', 'DEFAULT'

do 13 i=1,numx
do 14 j=1,numy
do 15 k=1,numz

	write(88,*) DefectListVTK(i,j,k,2)

15 continue
14 continue
13 continue

write(88,'(a,a,a,I4)') 'SCALARS ', 'SIAs', ' float', 1
write(88,'(a,a)') 'LOOKUP_TABLE ', 'DEFAULT'

do 16 i=1,numx
do 17 j=1,numy
do 18 k=1,numz

	write(88,*) DefectListVTK(i,j,k,3)

18 continue
17 continue
16 continue

close(88)

else

!Send defect information to master
call MPI_SEND(numCells, 1, MPI_INTEGER, MASTER, 567, MPI_COMM_WORLD, ierr)

do 23 i=1,numCells
	
	!Step 5.1: identify the x,y,z index of this cell
	cellIndex(1)=(myMesh(i)%coordinates(1)-myProc%globalCoord(1))/myMesh(1)%length+1
	cellIndex(2)=(myMesh(i)%coordinates(2)-myProc%globalCoord(3))/myMesh(1)%length+1
	cellIndex(3)=(myMesh(i)%coordinates(3)-myProc%globalCoord(5))/myMesh(1)%length+1
	
	!Step 5.2: count number of vacancies, interstitials, and He atoms in this cell
	defectCurrent=>defectList(i)
	
	numV=0
	numSIA=0
	numHe=0
	
	do 24 while(associated(defectCurrent))
		numV=numV+defectCurrent%defectType(2)*defectCurrent%num
		numHe=numHe+defectCurrent%defectType(1)*defectCurrent%num
		numSIA=numSIA+defectCurrent%defectType(3)*defectCurrent%num + &
			defectCurrent%defectType(4)*defectCurrent%num
		
		defectCurrent=>defectCurrent%next
	24 continue
	
	!Step 5.3: Send information to master
	call MPI_SEND(cellIndex, 3, MPI_INTEGER, MASTER, 571, MPI_COMM_WORLD, ierr)
	call MPI_SEND(numHe, 1, MPI_INTEGER, MASTER, 568, MPI_COMM_WORLD, ierr)
	call MPI_SEND(numV, 1, MPI_INTEGER, MASTER, 569, MPI_COMM_WORLD, ierr)
	call MPI_SEND(numSIA, 1, MPI_INTEGER, MASTER, 570, MPI_COMM_WORLD, ierr)
	
23 continue

endif

nullify(defectCurrent)

end subroutine

!***************************************************************************************************
!
!> Subroutine outputRates(step) - outputs reaction rates to a file
!!
!! Outputs the total reaction rate in each processor to a file. Used for debugging and for parallel
!! analysis
!!
!! Inputs: integer step (the reaction step number)
!! Outputs: reaction rate of each processor written in file
!
!****************************************************************************************************

subroutine outputRates(elapsedTime, step)
use mod_srscd_constants
use DerivedType
implicit none

include 'mpif.h'

integer i, step, status(MPI_STATUS_SIZE)
double precision rate(myProc%numtasks), rateTemp, elapsedTime

if(myProc%taskid==MASTER) then

	!Record master reaction rate
	rate(1)=totalRate
	
	do 10 i=1,myProc%numtasks-1
		
		!Recieve data from other procs
		!record data from other procs in rate()
		
		call MPI_RECV(rateTemp,1,MPI_DOUBLE_PRECISION,i,step,MPI_COMM_WORLD,status,ierr)
		rate(i+1)=rateTemp
		
	10 continue
	
	!Output data from other procs to file
	
	write(85,*) 'step', step, 'rates', (rate(i), i=1,myProc%numtasks)
	
else

	!send reaction rate to master proc
	call MPI_SEND(totalRate, 1, MPI_DOUBLE_PRECISION, MASTER, step, MPI_COMM_WORLD, ierr)
	
endif

end subroutine
