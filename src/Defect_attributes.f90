! $Header: /home/CVS//srscd/src/Defect_attributes.f90,v 1.9 2015/04/02 15:44:44 aydunn Exp $

!*****************************************************************************************
!>double precision function find diffusivity - returns the diffusivity of a given defect type
!!
!! This function looks up the diffusivity of a given defect type using input date from 
!! material parameter input file. It either looks up diffusivity from values in a list or
!! computes diffusivity using diffusivityCompute in the case of a functional form.
!!
!!Input: defect type
!!Output: diffusivity (nm^2/s)
!*****************************************************************************************

double precision function findDiffusivity(DefectType)
use DerivedType
use mod_srscd_constants
implicit none

integer defectType(numSpecies)
integer i, j, numSame
double precision Diff
double precision DiffusivityCompute

!***************************************************************************************************
!This function returns the diffusivity of defect DefectType.
!It searches to see if DefectType is listed in DiffSingle, and if not, if it is listed in DiffFunction.
!If it is in neither, it outputs an error message (defect type should not exist)
!***************************************************************************************************

do 10 i=1,numSingleDiff
	numSame=0
	do 11 j=1,numSpecies
		if(DefectType(j)==DiffSingle(i)%defectType(j)) then
			numSame=numSame+1
		endif
	11 continue
	if (numSame==numSpecies) then
		Diff=DiffSingle(i)%D*dexp(-DiffSingle(i)%Em/(kboltzmann*temperature))
		exit
	endif
10 continue

if(i==numSingleDiff+1) then	!did not find defect in single defect list
	do 12 i=1,numFuncDiff
		numSame=0
		do 13 j=1,numSpecies
			if(DefectType(j)==0 .AND. DiffFunc(i)%defectType(j)==0) then
				numSame=numSame+1
			else if(DefectType(j) .NE. 0 .AND. DiffFunc(i)%defectType(j)==1) then
				if(DefectType(j) .GE. DiffFunc(i)%min(j)) then
				if(DefectType(j) .LE. DiffFunc(i)%max(j) .OR. DiffFunc(i)%max(j)==-1) then
					numSame=numSame+1
				endif
				endif
			endif
		13 continue
		if(numSame==numSpecies) then
			Diff=DiffusivityCompute(DefectType, DiffFunc(i)%functionType, DiffFunc(i)%numParam,&
				DiffFunc(i)%parameters)
				exit
		endif
	12 continue
	if(i==numFuncDiff+1) then
		write(*,*) 'error defect diffusion not allowed'
		write(*,*) DefectType
		Diff=0d0
	endif
endif

findDiffusivity=Diff
end function

!*****************************************************************************************
!>double precision function diffusivityCompute - computes diffusivity using a functional form
!! for defects that don't have their diffusivity given by a value in a list.
!!
!! This function has several hard-coded functional forms for diffusivity, including immobile
!! defects, constant functions, and mobile SIA loops. Additional functional forms can be added
!! as needed.
!*****************************************************************************************

double precision function DiffusivityCompute(DefectType, functionType, numParameters, parameters)
use mod_srscd_constants
implicit none

integer DefectType(numSpecies)
integer functionType, numParameters
double precision parameters(numParameters)
double precision Diff

!***************************************************************************************************
!This function computes diffusivity using functional form and parameters given in the input file
!***************************************************************************************************

if(functionType==1) then
	!used for immobile defects
	Diff=0d0
else if(functionType==2) then
	!used for constant functions
	Diff=parameters(1)
else if(functionType==3) then
	!Mobile SIA loop diffusivity
	Diff=(parameters(1)+parameters(2)/(dble(DefectType(3))**(parameters(3))))*&
		exp(-(parameters(4)+parameters(5)/dble(DefectType(3))**(parameters(6)))/(kboltzmann*temperature))
else
	write(*,*) 'error incorrect diffusivity function chosen'
endif

DiffusivityCompute=Diff

end function

!***************************************************************************************************
!This function returns the binding energy of defect DefectType() which releases defect product().
!It searches to see if DefectType and product are listed in BindSingle, and if not, in BindFunc.
!If they are in neither, it outputs an error message (cannot dissociate that product from that defect)
!***************************************************************************************************

!*****************************************************************************************
!>double precision function find binding - returns the binding energy of a given defect type
!!
!! This function looks up the binding energy of a given defect type using input data from 
!! material parameter input file. It either looks up binding energy from values in a list or
!! computes binding energy using bindingCompute in the case of a functional form.
!!
!!Input: defect type, product type (what type of defect dissociates from the cluster)
!!Output: binding energy (eV)
!*****************************************************************************************

double precision function findBinding(DefectType, productType)
use DerivedType
use mod_srscd_constants
implicit none

integer DefectType(numSpecies), productType(numSpecies)
integer i, j, numSame, numSameProduct
double precision Eb
double precision BindingCompute

do 10 i=1,numSingleBind
	numSame=0
	numSameProduct=0
	do 11 j=1,numSpecies
		if(DefectType(j)==BindSingle(i)%defectType(j)) then
			numSame=numSame+1
		endif
		if(productType(j)==BindSingle(i)%product(j)) then
			numSameProduct=numSameProduct+1
		endif
	11 continue
	if (numSame==numSpecies .AND. numSameProduct==numSpecies) then
		Eb=BindSingle(i)%Eb
		exit
	endif
10 continue

if(i==numSingleBind+1) then	!did not find defect in single defect list
	do 12 i=1,numFuncBind
		numSame=0
		numSameProduct=0
		do 13 j=1,numSpecies
			if(DefectType(j)==0 .AND. BindFunc(i)%defectType(j)==0) then
				numSame=numSame+1
			else if(DefectType(j) .NE. 0 .AND. BindFunc(i)%defectType(j)==1) then
				if(DefectType(j) .GE. BindFunc(i)%min(j)) then
				if(DefectType(j) .LE. BindFunc(i)%max(j) .OR. BindFunc(i)%max(j)==-1) then
					numSame=numSame+1
				endif
				endif
			endif
			if(productType(j)==0 .AND. BindFunc(i)%product(j)==0) then
				numSameProduct=numSameProduct+1
			else if(productType(j) == 1 .AND. BindFunc(i)%product(j)==1) then	!used to find dissociation binding energy
				numSameProduct=numSameProduct+1
			else if(productType(j) .NE. 0 .AND. BindFunc(i)%product(j)==-1) then	!used to identify de-pinning binding energy
				numSameProduct=numSameProduct+1
			endif
		13 continue
		if(numSame==numSpecies .AND. numSameProduct==numSpecies) then
			
			Eb=BindingCompute(DefectType, productType, BindFunc(i)%functionType, BindFunc(i)%numParam,&
				BindFunc(i)%parameters)
			
				exit
		endif
	12 continue
	if(i==numFuncBind+1) then
		write(*,*) 'error dissociation reaction not allowed'
		write(*,*) DefectType
		write(*,*) ProductType
		Eb=0d0
	endif
endif

findBinding=Eb
end function

!*****************************************************************************************
!>double precision function bindingCompute - computes binding energy using a functional form
!! for defects that don't have their binding energy given by a value in a list.
!!
!! This function has several hard-coded functional forms for binding energy, including vacancy
!! clusters, SIA clusters, He/V clusters, and the activation energy for a sessile-glissile
!!SIA loop transformation
!*****************************************************************************************


double precision function BindingCompute(DefectType, product, functionType, numParameters, parameters)
use mod_srscd_constants
implicit none

integer DefectType(numSpecies), product(numSpecies)
integer functionType, numParameters, num, HeNum, VNum, SIANum, i
double precision parameters(numParameters)
double precision Eb, Eb_VOnly, Eb_HeV

!***************************************************************************************************
!This function computes diffusivity using functional form and parameters given in the input file
!***************************************************************************************************

if(functionType==1) then
	!used for zero functions
	Eb=0d0
else if(functionType==2) then
	!used for constant functions
	Eb=parameters(1)
else if(functionType==3) then
	!Mobile SIA loop diffusivity
	write(*,*) 'error no functionType 3 in BindingCompute'
else if(functionType==4) then
	num=0
	do 10 i=1,numSpecies
		if(DefectType(i) .GT. num) then
			num=DefectType(i)
		endif
	10 continue
	Eb=parameters(1)+(parameters(2)-parameters(1))*(dble(num)**(2d0/3d0)-dble(num-1)**(2d0/3d0))/(2d0**(2d0/3d0)-1d0)
else if(functionType==5) then
	write(*,*) 'error no functionType 5 in BindingCompute'
else if(functionType==6) then
	HeNum=DefectType(1)
	VNum=DefectType(2)	
	if(dble(HeNum)/dble(VNum) .LE. .5d0) then
		!helium cannot dissociate from HeV clusters with mostly V
		Eb=10d0
	else
		!Use He_mV_n binding energy (does not apply for low m/n ratios)
		!Eb=parameters(1)-parameters(2)*dlog(dble(HeNum)/dble(VNum))/dlog(10d0)-&
		!	parameters(3)*dlog(dble(HeNum)/dble(VNum))**2d0/(dlog(10d0)**2d0) !Marion and Bulatov, from Terentyev
		
		!EDIT: 2014.10.08: Helium cannot dissociate from HeV clusters at all.
		Eb=10d0
		
		
		if(Eb .LT. 0d0) then
			Eb=0d0
		endif
	
	endif
else if(functionType==7) then
	HeNum=DefectType(1)
	VNum=DefectType(2)

!	if(dble(HeNum)/dble(VNum) .LE. .5d0) then

!		!use vacancy cluster binding energy
!		Eb=parameters(1)+(parameters(2)-parameters(1))*(dble(VNum)**(2d0/3d0)-dble(VNum-1)**(2d0/3d0))/(2d0**(2d0/3d0)-1d0)
		
!		if(Eb .LT. 0d0) then
!			Eb=0d0
!		endif

!	else
	
		!vacancy cluster binding energy
		Eb_VOnly=parameters(1)+(parameters(2)-parameters(1))*(dble(VNum)**(2d0/3d0)-dble(VNum-1)**(2d0/3d0))/(2d0**(2d0/3d0)-1d0)
		
		if(Eb_VOnly .LT. 0d0) then
			Eb_VOnly=0d0
		endif

		!He_mV_n binding energy (does not apply for low m/n ratios)
		!Eb_HeV=parameters(3)+parameters(4)*dlog(dble(HeNum)/dble(VNum))/dlog(10d0)+&
		!	parameters(5)*dlog(dble(HeNum)/dble(VNum))**2d0/(dlog(10d0)**2d0) !Marion and Bulatov, from Terentyev
		Eb_HeV=parameters(3)*parameters(4)**(dlog(dble(HeNum)/dble(VNum))/dlog(10d0))+parameters(5)
		
		if(Eb_HeV .LT. 0d0) then
			Eb_HeV=0d0
		endif
		
		!Modification 2015.03.19: choose the larger binding energy between the V_only functional form and the He_V functional form
	
!		if(Eb_VOnly .GT. Eb_HeV) then
!			Eb=Eb_VOnly+1d0
!		else
!			Eb=Eb_HeV+1d0
!		endif

		Eb=Eb_VOnly+Eb_HeV
		
!	endif
	
else if(functionType==8) then
	
	!SIA sessile - glissile binding energy
	!Using (made-up) empirical functional form
	SIANum=DefectType(4)
	
	!2/3 power law
	!Eb=parameters(1)-parameters(2)*(dble(SIANum)**(2d0/3d0)-dble(SIANum-1)**(2d0/3d0))
	
	!linear binding energy dependence
	Eb=parameters(1)*SIANum+parameters(2)

else
	write(*,*) 'error incorrect Eb function chosen'
endif

BindingCompute=Eb

end function

!*****************************************************************************************
!>integer function findDefectSize - returns the size of a cluster
!!
!!This function will find the effective size of the defect (hard-coded information), used for determining
!!the radius of the defect (for dissociation and clustering reactions).
!!It returns n, the number of lattice spaces taken up by this defect.
!!
!!NOTE: for He_nV_m clusters, this function returns the larger of m or n
!*****************************************************************************************

integer function findDefectSize(defectType)
use mod_srscd_constants
implicit none

integer defectType(numSpecies), max, i

!Hard-coded below and may be changed if the rules for defect size change.
max=0
do 10 i=1, numSpecies
	if(defectType(i) .GT. max) then
		max=defectType(i)
	endif
10 continue

findDefectSize=max
end function
