! $Header: /home/CVS//srscd/src/SRSCD_par.f90,v 1.22 2015/04/02 15:44:45 aydunn Exp $

!***************************************************************************************************
!>Main program
!!
!!For outline, see main page of manual. This program uses the kinetic Monte Carlo algorithm
!!in a synchronous parallel implementation to simulate radiation damage accumulation and
!!subsequent annealing in metals.
!!
!!Several debugging options can be triggered by un-commenting them in this program, in the
!!case of difficulty.
!***************************************************************************************************

program SRSCD
use DerivedType			!variable classes for SRSCD
use MeshReader			!module created for reading in mesh
use mod_srscd_constants		!module containing all global variables
use randdp				!module for double precision random number generation
implicit none
include 'mpif.h'

type(defect), pointer :: defectCurrent									!used to find defects
type(defectUpdateTracker), pointer :: defectUpdate, defectUpdateCurrent !used to update reactions
type(reaction), pointer :: reactionCurrent								!used to find reactions
type(cascade), pointer :: CascadeCurrent								!used to find defects/reactions in fine mesh

double precision elapsedTime, totalTime, tau, GenerateTimestep, TotalRateCheck
integer status(MPI_STATUS_SIZE), step, annealIter, sim, numDefectsRecv, tracker, outputCounter, nullSteps
integer cascadeCell, i, j, k
integer, allocatable :: cellRecvTot(:), defectRecvTot(:,:)
real time1, time2

integer CascadeCount, TotalCascades !<Used to count the number of cascades present in the simulation

character(12) filename, filename2, filename3, filename4, filename5, filename6

double precision rateDiff	!temporary
type(Reaction), pointer :: reactionTemp

interface
	subroutine chooseReaction(reactionCurrent, CascadeCurrent)
	use DerivedType
	type(reaction), pointer :: reactionCurrent
	type(cascade), pointer :: CascadeCurrent
	end subroutine
	
	subroutine addCascadeExplicit(reactionCurrent)
	use DerivedType
	type(reaction), pointer :: reactionCurrent
	end subroutine
	
	subroutine updateDefectList(reactionCurrent, defectUpdate, CascadeCurrent)
	use DerivedType
	type(reaction), pointer :: reactionCurrent
	type(DefectUpdateTracker), pointer :: defectUpdate
	type(cascade), pointer :: CascadeCurrent
	end subroutine
	
	subroutine updateReactionList(defectUpdate)
	use DerivedType
	type(DefectUpdateTracker), pointer :: defectUpdate
	end subroutine
	
	subroutine debugPrintReaction(reactionCurrent, step)
	use DerivedType
	type(reaction), pointer :: reactionCurrent
	integer step
	end subroutine
	
	subroutine debugPrintDefectUpdate(defectUpdate)
	use DerivedType
	type(defectUpdateTracker), pointer :: defectUpdate
	end subroutine
	
	subroutine releaseFineMeshDefects(CascadeCurrent)
	use DerivedType
	type(cascade), pointer :: CascadeCurrent
	end subroutine
	
	subroutine DEBUGcheckForUnadmissible(reactionCurrent, step)
	use DerivedType
	type(Reaction), pointer :: reactionCurrent
	integer step
	end subroutine
end interface

call cpu_time(time1)

open(81, file='parameters.txt',action='read', status='old')

!Initialize MPI interface

call MPI_INIT(ierr)
call MPI_COMM_SIZE(MPI_COMM_WORLD, myProc%numtasks, ierr)		!read number of processors
call MPI_COMM_RANK(MPI_COMM_WORLD, myProc%taskid, ierr)			!read processor ID of this processor

call initializeMesh()			!open mesh file and carry out parallel mesh initialization routine
call selectMaterialInputs()		!open material input files and read all relevant data (migration and binding energies, etc)
call readCascadeList()			!input cascade list from file to cascadeList (global variable)
call readImplantData()			!input (1-dimensional) non-uniform defect implantation info (DPA rates, He implant rates)
call readParameters()			!read simulation parameters (DPA rate, temperature, etc)


!Create fine mesh connectivity
allocate(cascadeConnectivity(numCellsCascade, 6))
call createCascadeConnectivity()

!***********************************************************************
!For running multiple simulations, initialize the defects, reactions,
!boundary, etc. and loop here.
!***********************************************************************

do 13 sim=1,numSims

!Reset the temperature to tempStore at the beginning of each simulation
temperature=tempStore

!Initialize output files
if(myProc%taskid==MASTER) then
	filename(1:6)='rawdat'
	filename2(1:6)='totdat'
	filename3(1:6)='postpr'
	filename4(1:6)='rxnrat'
	!filename5(1:6)='debugg'
	!filename6(1:6)='defect'
	write(unit=filename(7:8), fmt='(I2)') sim
	write(unit=filename2(7:8), fmt='(I2)') sim
	write(unit=filename3(7:8), fmt='(I2)') sim
	write(unit=filename4(7:8), fmt='(I2)') sim
	!write(unit=filename5(7:8), fmt='(I2)') sim
	!write(unit=filename6(7:8), fmt='(I2)') sim
	filename(9:12)='.out'
	filename2(9:12)='.out'
	filename3(9:12)='.out'
	filename4(9:12)='.out'
	!filename5(9:12)='.log'
	!filename6(9:12)='.xyz'
	open(82, file=filename, action='write', status='Unknown')
	open(83, file=filename2, action='write', status='Unknown')
	open(84, file=filename3, action='write', status='Unknown')
	open(85, file=filename4, action='write', status='Unknown')
	!open(86, file=filename5, action='write', status='Unknown')
	!open(87, file=filename6, action='write', status='Unknown')
endif

!filename5(1:6)='debugg'
!write(unit=filename5(7:8), fmt='(I2)') myProc%taskid
!filename5(9:12)='.log'
!open(86, file=filename5, action='write', status='Unknown')

if(myProc%taskid==MASTER) then
	write(*,*) 'Initializing trial', sim, 'proc', myProc%taskid, 'temp', temperature
endif

!NEXT STEPS:
!3. Choose Reaction
!4. Carry out reaction (update defect)->note all relevant defects that have been changed
!5. Update reaction lists->use defects deleted and defects added (reactants and products) to update relevant reactions
!	(first, delete all reactions associated with the REACTANTS AND PRODUCTS in the cell and all DIFFUSION reactions associated with
!	reactants in neighboring cells. Then add all reaction rates associated with REACTANTS AND PRODUCTS in the cell and diffusion 
!	reactions in neighboring cells.)
!6. Calculate total reaction rate and communicate using MPI_GATHER the total reaction rate.
!7. Repeat 2-6

!*******
!Initializing reaction lists and defect lists
!*******

!Here we initialize the random number generators on each processor with a different seed. This 
!is done by creating random integers on the master processor and sending them to the slaves as
!random number seeds.

call initializeRandomSeeds()		!set unique random number seeds in each processor
allocate(DefectList(numCells))
allocate(reactionList(numCells))
call initializeDefectList()			!initialize defects within myMesh
call initializeBoundaryDefectList()		!initialize defects on boundary of myMesh (in other procs)
call initializeReactionList()		!initialize reactions within myMesh
call initializeTotalRate()			!initialize totalRate and maxRate using reactionList(:)
call initializeDebugRestart()		!input defects into coarse mesh from restart file (for debugging)

!call DEBUGPrintReactionList(0)

!******************************************************************
!Initialize Counters
!******************************************************************

if(debugToggle=='yes') then
	
	!If we are restarting from a reset file, then we have to start wtih a
	!nonzero dpa and at a nonzero time. All of the 'old' implant events
	!and He implant events are tracked in the master processor.
	
	if(myProc%taskid==MASTER) then
		numImplantEvents	= numImplantEventsReset
		numHeImplantEvents	= numHeImplantEventsReset
	else
		numImplantEvents	= 0
		numHeImplantEvents	= 0
	endif
	elapsedTime			= elapsedTimeReset
	!Reset the reactions within this cell and diffusion to neighboring
	!cells in the same processor
	do 14 i=1,numCells
		call resetReactionListSingleCell(i)
	14 continue

	!Create the boundary defects in this processor and send boundary
	!defects to neighboring processor. Update reactions for diffusion
	!into boundary (into other processors)
	
	!NOTE: this will crash if the number of cells in a neighboring processor
	!is not the same as in the local processor. Easy fix, but putting
	!off until later.
	
	do 15 i=1,numCells
		call cascadeUpdateStep(i)
	15 continue
	
	write(*,*) 'Processor', myProc%taskid, 'Defect lists and reaction lists initialized'
	
else
	numImplantEvents	= 0
	numHeImplantEvents	= 0
	numAnnihilate		= 0
	elapsedTime			= 0d0
endif

step=0
nullSteps=0

totalTime=totalDPA/DPARate
TotalCascades=0
outputCounter=0
nullify(ActiveCascades)

do 10 while(elapsedTime .LT. totalTime)
	
	step=step+1
	
	!Logical variable tells us whether cascade communication step needs to be carried out
	!(0=no cascade, nonzero=number of volume element where cascade event has happened)
	cascadeCell=0
	
	call MPI_ALLREDUCE(totalRate,maxRate,1,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD,ierr)

!*************************************************************************
!	if(mod(step,10)==0) then
!		!Debugging subroutine: outputs the reaction rate in each processor
!		call outputRates(step)
!	endif
!*************************************************************************
	
	!***********************************************************************************************
	!Choose from reactions in in reactionList(:) (local to this processor). Null events possible.
	!
	!If explicit implant scheme has been chosen, implant cascades when elapsed time has passed 
	!a threshold, and do not iterate timestep when doing so.
	!***********************************************************************************************
	
	!If implantScheme=='explicit', cascade implantation needs to be carried out explicitly
	if(implantScheme=='explicit') then
		
		if(elapsedTime .GE. numImplantEvents*(numDisplacedAtoms*atomsize)/(totalVolume*DPARate)) then
			
			call addCascadeExplicit(reactionCurrent)
			
			!Do not generate a timestep in this case; this is an explicit (zero-time) reaction
			
			if(myProc%taskid==MASTER) then
				tau=0d0
			endif
			
		else
		
			call chooseReaction(reactionCurrent, CascadeCurrent)
			
			!Generate timestep in the master processor and send it to all other processors
			
			if(myProc%taskid==MASTER) then
				tau=GenerateTimestep()
			endif
			
		endif
		
	else if(implantScheme=='MonteCarlo') then
		
		call chooseReaction(reactionCurrent, CascadeCurrent)
		
		!Generate timestep in the master processor and send it to all other processors
		
		if(myProc%taskid==MASTER) then
			tau=GenerateTimestep()
		endif
	
	else	
	
		write(*,*) 'error choosing reaction main program'
	
	endif
	
	!Update elapsed time based on tau, generated timestep. If cascade implant chosen in explicit scheme, tau=0d0
	if(myProc%taskid==MASTER) then
		
		elapsedTime=elapsedTime+tau
		
		!NOTE: we should eliminate this send/recieve pair and only track time in the master to save communication
		do 11 i=1,myProc%numtasks-1
			call MPI_SEND(elapsedTime, 1, MPI_DOUBLE_PRECISION,i,1,MPI_COMM_WORLD,ierr)
		11 continue

	else
		!slave processors recieve elapsed time from master
		call MPI_RECV(elapsedTime,1,MPI_DOUBLE_PRECISION,MASTER,1,MPI_COMM_WORLD,status,ierr)
	endif

	!***********************************************************************************************
	!Update defects according to reactions chosen. Communicate defects that have changed on 
	!boundaries of other processors and create a list of defects whose reaction rates must be update
	!in this processor due to defects updated in this processor and in neighboring processors.
	!
	!Update reactions by calculating all possible reaction rates that have changed and removing/adding
	!reactions to reaction lists accordingly. No communication is necessary at this step because 
	!all defects in neighboring cells in other processors are stored in myBoundary. This is totally local
	!
	!all of the debug subroutines are used to print various information for the sake of debugging.
	!***********************************************************************************************
		
!	call DEBUGPrintReaction(reactionCurrent, step)

	!************
	! Optional: count how many steps are null events
	!************

	if(.NOT. associated(reactionCurrent)) then
		nullSteps=nullSteps+1
	endif
	
	call updateDefectList(reactionCurrent, defectUpdate, CascadeCurrent)

!	call DEBUGPrintDefectUpdate(defectUpdate)

	!If a cascade is chosen, update reaction rates for all defects remaining in coarse mesh element
	
	if(associated(reactionCurrent)) then
		if(reactionCurrent%numReactants==-10) then
			
			!Resets reaction list in cell and neighbors (on same processor)
			call resetReactionListSingleCell(reactionCurrent%cellNumber(1))
			
			!Variable tells us whether cascade communication step needs to be carried out
			!and if so in what volume element in the coarse mesh
			cascadeCell=reactionCurrent%cellNumber(1)
			
		endif
	endif
	!Update reaction rates for defects involved in reaction chosen
	
	call updateReactionList(defectUpdate)
	
!	call DEBUGPrintDefects(step)
!	call DEBUGPrintReactionList(step)
!	call DEBUGCheckForUnadmissible(reactionCurrent, step)

	!If we have chosen an event inside a fine mesh, we check the total reaction rate within that
	!fine mesh. If the total rate is less than a set value, we assume the cascade is annealed and
	!release the defects into the coarse mesh.

	if(associated(CascadeCurrent)) then
		if(CascadeCurrent%totalRate .LT. cascadeReactionLimit) then
			
			!Record the coarse mesh cell number of cascade (it is destroyed in releaseFineMeshDefects)
			!Used to reset reaction list and to tell cascadeUpdateStep whether a cascade event has occurred
			cascadeCell=CascadeCurrent%cellNumber

			!Release cascade defects into coarse mesh cell and reset the reaction list within that cell
			call releaseFineMeshDefects(CascadeCurrent)

			call resetReactionListSingleCell(cascadeCell)
			
		endif
	endif
	
	!Cascade communication step:
	!Tell neighbors whether a cascade has occurred in a cell that is a boundary of a neighbor.
	!If so, update boundary mesh (send defects to boundary mesh) and update all diffusion reaction rates.

	call cascadeUpdateStep(cascadeCell)

	!Every time a cascade is added, we reset the total rate and check how wrong it has become
	if(associated(reactionCurrent)) then
		if(implantType=='Cascade') then
			if(reactionCurrent%numReactants==-10) then
				!if(myProc%taskid==MASTER) write(*,*) 'Checking Total Rate'
				totalRate=TotalRateCheck()
			endif
		elseif(implantType=='FrenkelPair') then
			if(mod(step,1000)==0) then
				totalRate=TotalRateCheck()
			endif
		endif
	endif
	
	!******************************************
	! Optional: count how many cascades are present at step i and compile to find avg. number
	! of cascades present per step
	!******************************************
	
	!TotalCascades=TotalCascades+CascadeCount()
	
	!******************************************
	! Output
	!******************************************
	
	if(elapsedTime .GT. totalTime/200d0*(2d0)**(outputCounter)) then
		
		call MPI_ALLREDUCE(numImplantEvents,totalImplantEvents, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
		call MPI_ALLREDUCE(numHeImplantEvents,numHeImplantTotal,1,MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
		
		DPA=dble(totalImplantEvents)/(((myProc%globalCoord(2)-myProc%globalCoord(1))*(myProc%globalCoord(4)-myProc%globalCoord(3))*&
			(myProc%globalCoord(6)-myProc%globalCoord(5)))/(numDisplacedAtoms*atomsize))
		
		if(myProc%taskid==MASTER) then
			call cpu_time(time2)
			write(*,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
			write(*,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
			write(84,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
			write(84,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
			
			!Optional: output average number of cascades present per step in local processor
			!write(84,*) 'Processor ', myProc%taskid, 'Avg. cascades present', dble(TotalCascades)/dble(step)
			
			!Optional: output fraction of steps that are null events
			write(84,*) 'Fraction null steps', dble(nullSteps)/dble(step)
			
			write(84,*)
			write(*,*)
		endif
		
		!Several defect output optionas available, these outputs should be chosen in input file (currently hard coded)
		
		call outputDefects()
		call outputDefectsTotal(elapsedTime, step)
		!call outputDefectsXYZ()
		!call outputDefectsVTK(outputCounter)
		
		outputCounter=outputCounter+1

	endif
	
!	if(mod(step,100000)==0) then
		
!		call MPI_ALLREDUCE(numImplantEvents,totalImplantEvents, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
!		call MPI_ALLREDUCE(numHeImplantEvents,numHeImplantTotal,1,MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
		
!		DPA=dble(totalImplantEvents)/(((myProc%globalCoord(2)-myProc%globalCoord(1))*(myProc%globalCoord(4)-myProc%globalCoord(3))*&
!			(myProc%globalCoord(6)-myProc%globalCoord(5)))/(numDisplacedAtoms*atomsize))
		
!		if(myProc%taskid==MASTER) then
!			call cpu_time(time2)
!			write(*,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
!			write(*,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
!				'computation time', time2-time1
!			write(*,*)
!		endif
		
		!***************************************************************************************
		!2014.10.13
		!
		! Adding debugging section, used to find errors in code that occur rarely
		!
		! Every 100000 steps, we will erase the debug file and open a new one in order to 
		! save memory. The debug file contains written outputs at major locations in the code,
		! in order to see where the code is hanging.
		!
		!***************************************************************************************
		
!		if(myProc%taskid==MASTER) then

!			Close and re-open write file (gets rid of old write data)
!			close(86)
!			open(86, file=filename5, action='write', status='Unknown')
			
!!			Write initial information into write file
!			write(86,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
!			write(86,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He Implant Events', numHeImplantTotal, &
!				'computation time', time2-time1
!			write(86,*)
			
!		endif
		
		
!	endif
	
!	if(mod(step,10)==0) then
!		call outputRates(elapsedTime, step)
!	endif
	
10 continue

!***********************************************************************
!Output defects at the end of the implantation loop
!***********************************************************************

call MPI_ALLREDUCE(numImplantEvents,totalImplantEvents, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
call MPI_ALLREDUCE(numHeImplantEvents,numHeImplantTotal,1,MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)

DPA=dble(totalImplantEvents)/(((myProc%globalCoord(2)-myProc%globalCoord(1))*(myProc%globalCoord(4)-myProc%globalCoord(3))*&
	(myProc%globalCoord(6)-myProc%globalCoord(5)))/(numDisplacedAtoms*atomsize))

if(myProc%taskid==MASTER) then
	call cpu_time(time2)

	write(*,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
	write(*,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
	write(84,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
	write(84,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
	
	!Optional: output average number of cascades present per step in local processor
	!write(84,*) 'Processor ', myProc%taskid, 'Avg. cascades present', dble(TotalCascades)/dble(step)
	
	!Optional: output fraction of steps that are null events
	write(84,*) 'Fraction null steps', dble(nullSteps)/dble(step)
	
	write(84,*) 
	write(*,*)
endif

call outputDefects()
call outputDefectsTotal(elapsedTime, step)
!call outputDefectsProfile(sim)
!call outputDefectsVTK(outputCounter)

!***********************************************************************
!Annealing loop: change the temperature to annealTemp and change all
!defect production rates to 0 (dpa rate, He implant rate)
!
!Then carry out simulation normally until elapsedTime=totalTime+annealTime
!***********************************************************************

if(annealTime .GT. 0d0) then
	call annealInitialization()	!Sets up simulation for annealing (change temp, etc)
endif

outputCounter=1		!Used to output once for each annealing step

if(myProc%taskid==MASTER .AND. annealTime .GT. 0d0) then
	write(*,*) 'Entering Annealing Phase'
	write(82,*) 'Entering Annealing Phase'
	write(83,*) 'Entering Annealing Phase'
	write(84,*) 'Entering Annealing Phase'
endif


!carry out annealing in multiple steps if desired. Each step increases temperature.
do 101 annealIter=1,annealSteps


if(annealType=='mult') then
	
	temperature=annealTemp*annealTempInc**dble(annealIter-1)

else if(annealType=='add') then

	temperature=annealTemp+dble(annealIter-1)*annealTempInc

else
	write(*,*) 'error unknown anneal type'
endif

do 111 i=1,numCells
	call resetReactionListSingleCell(i)
111 continue

totalRate=TotalRateCheck()

if(myProc%taskid==MASTER .AND. annealTime .GT. 0d0) then
	write(*,*) 'Anneal step', annealIter, 'temperature', temperature
	write(84,*) 'Anneal step', annealIter, 'temperature', temperature
	write(83,*) 'Anneal step', annealIter, 'temperature', temperature
endif


do 100 while(elapsedTime .LT. totalTime+dble(annealIter)*annealTime/dble(annealSteps))

	
	step=step+1
	
	!Logical variable tells us whether cascade communication step needs to be carried out
	!(0=no cascade, nonzero=number of volume element where cascade event has happened)
	cascadeCell=0
	
	call MPI_ALLREDUCE(totalRate,maxRate,1,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD,ierr)

!*************************************************************************
!	if(mod(step,10)==0) then
!		!Debugging subroutine: outputs the reaction rate in each processor
!		call outputRates(step)
!	endif
!*************************************************************************
	
	!***********************************************************************************************
	!Choose from reactions in in reactionList(:) (local to this processor). Null events possible.
	!***********************************************************************************************
		
	call chooseReaction(reactionCurrent, CascadeCurrent)
	
	!Generate timestep in the master processor and send it to all other processors
	
	if(myProc%taskid==MASTER) then
		tau=GenerateTimestep()
		if(elapsedTime-totalTime+tau .GT. annealTime/dble(annealSteps)*outputCounter) then
			!we have taken a timestep that moves us past this annealing step
			tau=annealTime/dble(annealSteps)*outputCounter-(elapsedTime-totalTime)
		endif
	endif
	
	!Update elapsed time based on tau, generated timestep. If cascade implant chosen in explicit scheme, tau=0d0
	
	if(myProc%taskid==MASTER) then
		
		elapsedTime=elapsedTime+tau
		
		!NOTE: we should eliminate this send/recieve pair and only track time in the master to save communication
		do 110 i=1,myProc%numtasks-1
			call MPI_SEND(elapsedTime, 1, MPI_DOUBLE_PRECISION,i,1,MPI_COMM_WORLD,ierr)
		110 continue

	else
		!slave processors recieve elapsed time from master
		call MPI_RECV(elapsedTime,1,MPI_DOUBLE_PRECISION,MASTER,1,MPI_COMM_WORLD,status,ierr)
	endif

	!***********************************************************************************************
	!Update defects according to reactions chosen. Communicate defects that have changed on 
	!boundaries of other processors and create a list of defects whose reaction rates must be update
	!in this processor due to defects updated in this processor and in neighboring processors.
	!
	!Update reactions by calculating all possible reaction rates that have changed and removing/adding
	!reactions to reaction lists accordingly. No communication is necessary at this step because 
	!all defects in neighboring cells in other processors are stored in myBoundary. This is totally local
	!
	!all of the debug subroutines are used to print various information for the sake of debugging.
	!***********************************************************************************************

!	call DEBUGPrintReaction(reactionCurrent, step)

	!************
	! Optional: count how many steps are null events
	!************
	
	if(.NOT. associated(reactionCurrent)) then
		nullSteps=nullSteps+1
	endif

	call updateDefectList(reactionCurrent, defectUpdate, CascadeCurrent)
	
!	call DEBUGPrintDefectUpdate(defectUpdate)

	!Update reaction rates for defects involved in reaction chosen
	
	call updateReactionList(defectUpdate)
	
!	call DEBUGPrintDefects(step)
!	call DEBUGPrintReactionList(step)
!	call DEBUGCheckForUnadmissible(reactionCurrent, step)
	
	!If we have chosen an event inside a fine mesh, we check the total reaction rate within that
	!fine mesh. If the total rate is less than a set value, we assume the cascade is annealed and
	!release the defects into the coarse mesh.
	
	if(associated(CascadeCurrent)) then
		if(CascadeCurrent%totalRate .LT. cascadeReactionLimit) then
			
			!Record the coarse mesh cell number of cascade (it is destroyed in releaseFineMeshDefects)
			!Used to reset reaction list and to tell cascadeUpdateStep whether a cascade event has occurred
			cascadeCell=CascadeCurrent%cellNumber

			!Release cascade defects into coarse mesh cell and reset the reaction list within that cell
			call releaseFineMeshDefects(CascadeCurrent)

			call resetReactionListSingleCell(cascadeCell)
			
		endif
	endif
		
	!Cascade communication step:
	!Tell neighbors whether a cascade has occurred in a cell that is a boundary of a neighbor.
	!If so, update boundary mesh (send defects to boundary mesh) and update all diffusion reaction rates.
	call cascadeUpdateStep(cascadeCell)
		
	!Update the totalRate in order to avoid any truncation error every 1000 steps (this value can be modified)
	
	if(mod(step,1000)==0) then
		totalRate=TotalRateCheck()
	endif
	
	!******************************************
	! Optional: count how many cascades are present at step i and compile to find avg. number
	! of cascades present per step
	!******************************************
	
	!TotalCascades=TotalCascades+CascadeCount()
	
	!******************************************
	! Output
	!******************************************
	
	if((elapsedTime-totalTime) .GE. annealTime/dble(annealSteps)*outputCounter) then
		
		call MPI_ALLREDUCE(numImplantEvents,totalImplantEvents, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
		call MPI_ALLREDUCE(numHeImplantEvents,numHeImplantTotal,1,MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
		
		DPA=dble(totalImplantEvents)/(((myProc%globalCoord(2)-myProc%globalCoord(1))*(myProc%globalCoord(4)-myProc%globalCoord(3))*&
			(myProc%globalCoord(6)-myProc%globalCoord(5)))/(numDisplacedAtoms*atomsize))
	
		if(myProc%taskid==MASTER) then
			call cpu_time(time2)
			write(*,*) 'time', elapsedTime, 'anneal time', elapsedTime-totalTime, 'dpa', dpa, 'steps', step
			write(*,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
			write(84,*) 'time', elapsedTime, 'anneal time', elapsedTime-totalTime, 'dpa', dpa, 'steps', step
			write(84,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
			
			!Optional: output average number of cascades present per step in local processor
			!write(*,*) 'Processor ', myProc%taskid, 'Avg. cascades present', dble(TotalCascades)/dble(step)
			!write(84,*) 'Processor ', myProc%taskid, 'Avg. cascades present', dble(TotalCascades)/dble(step)
			
			!Optional: output fraction of steps that are null events
			write(84,*) 'Fraction null steps', dble(nullSteps)/dble(step)
			
			write(84,*)
			write(*,*)
		endif
		
		call outputDefects()
		call outputDefectsTotal(elapsedTime, step)
		!call outputDefectsXYZ()
		!call outputDefectsVTK()
		
		outputCounter=outputCounter+1

	endif
	
!Anneal time loop
100 continue

!Multiple anneal steps loop
101 continue

!***********************************************************************
!Output final defect state
!***********************************************************************

call MPI_ALLREDUCE(numImplantEvents,totalImplantEvents, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
call MPI_ALLREDUCE(numHeImplantEvents,numHeImplantTotal,1,MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)

DPA=dble(totalImplantEvents)/(((myProc%globalCoord(2)-myProc%globalCoord(1))*(myProc%globalCoord(4)-myProc%globalCoord(3))*&
	(myProc%globalCoord(6)-myProc%globalCoord(5)))/(numDisplacedAtoms*atomsize))

!write(*,*) 'Fraction null steps', dble(nullSteps)/dble(step), 'Proc', myProc%taskid

if(myProc%taskid==MASTER) then
	call cpu_time(time2)
	
	write(*,*) 'Final Defect State'
	write(82,*) 'Final Defect State'
	write(83,*) 'Final Defect State'
	write(84,*) 'Final Defect State'
	
	write(*,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
	write(*,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
	
	write(84,*) 'time', elapsedTime, 'dpa', dpa, 'steps', step
	write(84,*) 'Cascades/Frenkel pairs', totalImplantEvents, 'He implant events', numHeImplantTotal, &
				'computation time', time2-time1
	
	!Optional: output average number of cascades present per step in local processor
	!write(*,*) 'Processor ', myProc%taskid, 'Avg. cascades present', dble(TotalCascades)/dble(step)
	!write(84,*) 'Processor ', myProc%taskid, 'Avg. cascades present', dble(TotalCascades)/dble(step)
	
	!Optional: output fraction of steps that are null events
	!write(*,*) 'Fraction null steps', dble(nullSteps)/dble(step)
	write(84,*) 'Fraction null steps', dble(nullSteps)/dble(step)
	
	write(84,*) 
	write(*,*)
endif

call outputDefects()
call outputDefectsTotal(elapsedTime, step)
!call outputDefectsProfile(sim)
!call outputDefectsXYZ()
!call outputDefectsVTK

call cpu_time(time2)

!***********************************************************************
!Final step: release all cascades into coarse mesh
!
!(deallocate all extra memory here)
!***********************************************************************

if(myProc%taskid==MASTER) then
	write(*,*) 'computation time', time2-time1
	write(*,*) 'total steps', step
	
	write(84,*) 'computation time', time2-time1
	write(84,*) 'total steps', step

	write(*,*) 'Deallocating memory: fine mesh defects and reactions'
endif

do 12 while(associated(ActiveCascades))
	CascadeCurrent=>ActiveCascades

	!Record the coarse mesh cell number of cascade (it is destroyed in releaseFineMeshDefects)
	!Used to reset reaction list and to tell cascadeUpdateStep whether a cascade event has occurred
	cascadeCell=CascadeCurrent%cellNumber

	!Release cascade defects into coarse mesh cell and reset the reaction list within that cell
	call releaseFineMeshDefects(CascadeCurrent)

	call resetReactionListSingleCell(cascadeCell)
	
12 continue

if(myProc%taskid==MASTER) then
	write(82,*) 'Released all fine mesh defects'
	write(83,*) 'Released all fine mesh defects'
	write(84,*) 'Released all fine mesh defects'
endif

!call outputDefects()
!call outputDefectsTotal(elapsedTime, step)

!Final memory cleanup: deallocate defects and reactions in coarse mesh

if(myProc%taskid==MASTER) then
	write(*,*) 'Deallocating memory: coarse mesh defects and reactions'
endif

call deallocateDefectList()

call deallocateReactionList()

call deallocateBoundarydefectList()

if(myProc%taskid==MASTER) then
	close(82)
	close(83)
	close(84)
	close(85)
	!close(86)
	!close(87)
endif

!End of loop for multiple trials
13 continue

deallocate(cascadeConnectivity)

if(myProc%taskid==MASTER) then
	write(*,*) 'Deallocating memory: cascade list'
endif

call deallocateCascadeList()

if(myProc%taskid==MASTER) then
	write(*,*) 'Deallocating material input data'
endif

call deallocateMaterialInput()

write(*,*) 'Finalizing processor', myProc%taskid

close(81)

call MPI_FINALIZE(ierr)		!must be performed at end of simulation to have no error

end program
